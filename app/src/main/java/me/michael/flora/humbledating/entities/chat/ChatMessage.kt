package me.michael.flora.humbledating.entities.chat

import me.michael.flora.p2pcommunication.entity.Message

data class ChatMessage(
    val message: Message,
    val isSent: Boolean
)