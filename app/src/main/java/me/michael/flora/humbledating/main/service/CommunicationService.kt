package me.michael.flora.humbledating.main.service

import android.app.PendingIntent
import android.app.Service
import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.content.IntentFilter
import android.os.Binder
import android.os.IBinder
import android.util.Log
import androidx.lifecycle.LifecycleService
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import me.michael.flora.humbledating.R
import me.michael.flora.humbledating.main.MainActivity
import me.michael.flora.humbledating.main.broadcastreceiver.BluetoothBroadcastReceiver
import me.michael.flora.humbledating.main.notification.NotificationsManager
import me.michael.flora.p2pcommunication.CommunicationManager
import me.michael.flora.p2pcommunication.entity.User
import me.michael.flora.p2pcommunication.entity.UserInformation


class CommunicationService: LifecycleService() {
    private val logTag = this::class.java.simpleName
    private val notificationsManager by lazy { NotificationsManager.getInstance(applicationContext) }
    private val communicationManager by lazy { CommunicationManager.getInstance(applicationContext) }
    private val bluetoothBroadcastReceiver = BluetoothBroadcastReceiver.newBuilder()
        .addOnBluetoothTurnedOffListener {
            leaveWirelessNetwork()
        }
        .build()
    private lateinit var userInformation: UserInformation

    private var isWirelessCommunicationActive = false

    val availableUsers: LiveData<List<User>>
        get() = communicationManager.availableUsers

    override fun onCreate() {
        super.onCreate()
        startForeground(System.currentTimeMillis().toInt(), notificationsManager.getForegroundCommunicationServiceNotification())
        startToNotifyAboutNewMessages()
        registerReceiver(bluetoothBroadcastReceiver, IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED))
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)

        intent?.getParcelableExtra<UserInformation>(USER_INFORMATION_EXTRA_KEY)?.run {
            userInformation = this
            enterWirelessNetwork(userInformation)
        } ?: stopSelf()

        return Service.START_NOT_STICKY
    }

    override fun onBind(intent: Intent): IBinder?{
        super.onBind(intent)
        return ServiceBinder()
    }

    override fun onDestroy() {
        super.onDestroy()
        stopForeground(true)
        unregisterReceiver(bluetoothBroadcastReceiver)
        communicationManager.leaveWirelessNetwork()
        Log.i(logTag, "Destroyed service")
    }

    fun updateUserInformation(userInformation: UserInformation){
        this.userInformation = userInformation
    }

    fun enterWirelessNetwork(userInformation: UserInformation){
        if(isWirelessCommunicationActive){
            return
        }

        communicationManager.enterWirelessNetwork(userInformation){ entered ->
            isWirelessCommunicationActive = entered
        }
    }

    fun leaveWirelessNetwork(){
        communicationManager.leaveWirelessNetwork()
        stopSelf()
    }

    private fun startToNotifyAboutNewMessages(){
        availableUsers.observe(
            this,
            Observer{ availableUsers ->
                availableUsers.forEachIndexed { index, user ->
                    user.communicationChannel.receivedMessage.observe(
                        this,
                        Observer{ receivedMessage ->

                            val actionIntent = Intent(this, MainActivity::class.java).apply {
                                flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
                                putExtra(NAVIGATION_ACTION_EXTRA_KEY, R.id.action_global_destination_chat)
                                putExtra(USER_INDEX_EXTRA_KEY, index)
                            }

                            val pendingIntent = PendingIntent.getActivity(this, 1, actionIntent, 0)

                            notificationsManager.showNewMessageNotification(receivedMessage, pendingIntent)
                        }
                    )
                }
            }
        )
    }

    inner class ServiceBinder: Binder(){
        val service: CommunicationService = this@CommunicationService
    }

    companion object{
        const val USER_INFORMATION_EXTRA_KEY = "user_information_extra_key"
        const val NAVIGATION_ACTION_EXTRA_KEY = "destination"
        const val USER_INDEX_EXTRA_KEY = "user_index"
    }
}