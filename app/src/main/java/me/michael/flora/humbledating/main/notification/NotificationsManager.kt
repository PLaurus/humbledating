package me.michael.flora.humbledating.main.notification

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import me.michael.flora.humbledating.R
import me.michael.flora.humbledating.main.MainActivity
import me.michael.flora.p2pcommunication.entity.Message

class NotificationsManager(private val applicationContext: Context) {
    private val notificationService = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

    init {
        createNotificationChannels()
    }

    fun getForegroundCommunicationServiceNotification(): Notification{
        return NotificationCompat.Builder(applicationContext, FOREGROUND_COMMUNICATION_CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_p2p_communication_black_24dp)
            .setContentText(applicationContext.getString(R.string.foreground_communication_service_notification_description))
            .setContentIntent(PendingIntent.getActivity(applicationContext, 0, Intent(applicationContext,MainActivity::class.java), 0))
            .setAutoCancel(true)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .build()
    }

    fun showNewMessageNotification(message: Message, actionIntent: PendingIntent){
        val notification = NotificationCompat.Builder(applicationContext, MESSAGES_CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_message_white_24dp)
            .setContentTitle(message.senderName)
            .setContentText(message.content)
            .setStyle(NotificationCompat.BigTextStyle().bigText(message.content))
            .setContentIntent(actionIntent)
            .setAutoCancel(true)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .build()

        notificationService.notify(0, notification)
    }

    private fun createNotificationChannels(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createForegroundCommunicationServiceNotificationChannel()
            createNewMessagesNotificationChannel()
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createForegroundCommunicationServiceNotificationChannel(){
        val channelName = applicationContext.getString(R.string.foreground_communication_service_channel_name)
        val channelDescription = applicationContext.getString(R.string.foreground_communication_service_channel_description_name)
        val channelImportance = NotificationManager.IMPORTANCE_HIGH
        val channel = NotificationChannel(FOREGROUND_COMMUNICATION_CHANNEL_ID, channelName, channelImportance).apply {
            description = channelDescription
        }

        notificationService.createNotificationChannel(channel)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNewMessagesNotificationChannel(){
        val channelName = applicationContext.getString(R.string.messages_notification_channel_name)
        val channelDescription = applicationContext.getString(R.string.messages_notification_channel_description)
        val channelImportance = NotificationManager.IMPORTANCE_DEFAULT
        val channel = NotificationChannel(MESSAGES_CHANNEL_ID, channelName, channelImportance).apply {
            description = channelDescription
        }

        notificationService.createNotificationChannel(channel)
    }

    companion object{
        private const val MESSAGES_CHANNEL_ID = "new_message_channel"
        private const val FOREGROUND_COMMUNICATION_CHANNEL_ID = "foreground_communication_channel_id"

        private var singleton: NotificationsManager? = null

        fun getInstance(applicationContext: Context): NotificationsManager{
            val instance = singleton ?: NotificationsManager(applicationContext)

            singleton = instance

            return instance
        }
    }
}