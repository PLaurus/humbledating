package me.michael.flora.humbledating.main.destination.home.destination.nearbyusers

import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.AppCompatTextView
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import me.michael.flora.humbledating.R
import me.michael.flora.humbledating.main.MainActivity
import me.michael.flora.humbledating.main.destination.BaseFragment
import me.michael.flora.humbledating.main.destination.home.adapter.UsersListAdapter
import me.michael.flora.humbledating.main.viewmodel.MainViewModel
import me.michael.flora.p2pcommunication.entity.User

class NearbyUsersFragment: BaseFragment(R.layout.fragment_nearby_users) {
    private val mainViewModel by activityViewModels<MainViewModel>()

    private var usersRecyclerView: RecyclerView? = null
    private var noAvailableDevicesMessage: AppCompatTextView? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        findAllViews()
        initializeAllViews()
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
    }

    private fun findAllViews(){
        view?.run{
            usersRecyclerView = findViewById(R.id.nearby_users_list)
            noAvailableDevicesMessage = findViewById(R.id.nearby_users_no_available_users_message)
        }
    }

    private fun initializeAllViews(){
        initializeUsersList()
        manageViewsVisibility()
    }

    private fun initializeUsersList(){
        usersRecyclerView?.apply {
            val homeNavigationController = (activity as? MainActivity)?.navigationController
            val usersListAdapter = UsersListAdapter(mainViewModel, homeNavigationController)

            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = usersListAdapter

            mainViewModel.availableUsers.observe(
                this@NearbyUsersFragment as LifecycleOwner,
                Observer { availableUsers ->
                    usersListAdapter.setNewUsers(availableUsers)
                }
            )
        }
    }

    private fun manageViewsVisibility(){
        mainViewModel.availableUsers.observe(
            this@NearbyUsersFragment as LifecycleOwner,
            Observer<List<User>> { availableUsers ->
                if(availableUsers?.isNotEmpty() == true){
                    noAvailableDevicesMessage?.visibility = View.GONE
                    usersRecyclerView?.visibility = View.VISIBLE
                } else{
                    noAvailableDevicesMessage?.visibility = View.VISIBLE
                    usersRecyclerView?.visibility = View.GONE
                }
            }
        )
    }

}