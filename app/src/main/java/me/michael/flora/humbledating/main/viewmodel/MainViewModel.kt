package me.michael.flora.humbledating.main.viewmodel

import android.app.Application
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import me.michael.flora.humbledating.main.data.PreferencesRepository
import me.michael.flora.humbledating.main.service.CommunicationService
import me.michael.flora.p2pcommunication.entity.User
import me.michael.flora.p2pcommunication.entity.UserInformation


class MainViewModel(application: Application) : AndroidViewModel(application) {
    private val logTag = this::class.java.simpleName
    private val applicationContext: Context
        get() = getApplication()
    private val communicationIntent by lazy{ Intent(applicationContext, CommunicationService::class.java) }







    private val preferencesRepository = PreferencesRepository.getInstance(applicationContext)

    private val mutableSystemMessage = MutableLiveData<String>()
    private val mutableIsGooglePlayServicesAvailable = MutableLiveData<Boolean>()
    private val mutableAreNearbyUseConditionsMet = MutableLiveData<Boolean>()
    private val mutableUserInformation = MutableLiveData(preferencesRepository.getUserInformation())
    private val mutableIsCommunicationServiceAvailable = MutableLiveData<Boolean>()
    private val mutableIsUserOnline = MutableLiveData<Boolean>()

    private var isLastUserOnlineStateChangedAutomaticaly = false

    val communicationServiceBindingCallback = object: ServiceConnection {

        var communicationService: CommunicationService? = null

        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            communicationService = (service as CommunicationService.ServiceBinder?)?.service
            mutableIsCommunicationServiceAvailable.value = true

            communicationService?.run {
                addAvailableUsersSource(this)
            }
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            mutableIsCommunicationServiceAvailable.value = false
            communicationService?.run {
                removeAvailableUsersSource(this)
            }
            communicationService = null
        }
    }

    val systemMessage: LiveData<String> = mutableSystemMessage
    val isGooglePlayServicesAvailable: LiveData<Boolean> = mutableIsGooglePlayServicesAvailable
    val areNearbyUseConditionsMet = mutableAreNearbyUseConditionsMet
    val userInformation: LiveData<UserInformation?> = mutableUserInformation
    val isCommunicationServiceAvailable: LiveData<Boolean> = mutableIsCommunicationServiceAvailable
    val availableUsers = MediatorLiveData<List<User>>()
    val isUserOnline: LiveData<Boolean> = mutableIsUserOnline



    fun changeGooglePlayServicesAvailabilityState(state: Boolean){
        mutableIsGooglePlayServicesAvailable.value = state
    }

    fun setNearbyUseConditionsMet(met: Boolean = true){
        mutableAreNearbyUseConditionsMet.value = met
    }

    fun updateUserInformation(userInformation: UserInformation){
        mutableUserInformation.value = userInformation
    }

    fun goOnlineByUserAction(){
        if(!checkUserProfileExists()) return

        if(enterNetwork()){
            changeOnlineStatusByUserAction(newStatus = true)
        }
    }

    fun goOnlineBySystemAction(){
        if(!checkUserProfileExists()) return

        if(enterNetwork()){
            changeOnlineStatusBySystemAction(newStatus = true)
        }
    }

    fun goOfflineByUserAction(){
        if(leaveNetwork()){
            changeOnlineStatusByUserAction(newStatus = false)
        }
    }

    fun goOfflineBySystemAction(){
        if(leaveNetwork()){
            changeOnlineStatusBySystemAction(newStatus = false)
        }
    }

    private fun checkUserProfileExists(): Boolean{
        if(userInformation.value == null){
            mutableSystemMessage.value = "Заполните профиль, чтобы войти в сеть"
            return false
        }

        return true
    }

    private fun enterNetwork(): Boolean{
        userInformation.value?.let {
            applicationContext.startService(communicationIntent.putExtra(CommunicationService.USER_INFORMATION_EXTRA_KEY, it))
            return true
        }

        return false
    }

    private fun leaveNetwork(): Boolean{
        applicationContext.stopService(communicationIntent)
        return true
    }

    private fun addAvailableUsersSource(communicationService: CommunicationService){
        try{
            availableUsers.addSource(communicationService.availableUsers) { newUsers ->
                availableUsers.value = newUsers
            }
        } catch(ex: IllegalArgumentException){
            Log.e(logTag, "Available users source was added already", ex)
        }

    }

    private fun removeAvailableUsersSource(communicationService: CommunicationService){
        availableUsers.value = emptyList()
        availableUsers.removeSource(communicationService.availableUsers)
    }

    private fun changeOnlineStatusByUserAction(newStatus: Boolean){
        changeOnlineStatus(newStatus)
        isLastUserOnlineStateChangedAutomaticaly = false
    }

    private fun changeOnlineStatusBySystemAction(newStatus: Boolean){
        changeOnlineStatus(newStatus)
        isLastUserOnlineStateChangedAutomaticaly = true
    }

    private fun changeOnlineStatus(isOnline: Boolean){
        mutableIsUserOnline.value = isOnline
    }
}