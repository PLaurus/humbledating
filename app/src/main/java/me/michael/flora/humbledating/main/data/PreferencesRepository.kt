package me.michael.flora.humbledating.main.data

import android.content.Context
import androidx.core.content.edit
import me.michael.flora.p2pcommunication.entity.UserInformation
import me.michael.flora.p2pcommunication.entity.UserSex
import java.util.*

class PreferencesRepository private constructor(context: Context) {
    private val sharedPreferences = context.getSharedPreferences(PREFERENCES_FILE_NAME, Context.MODE_PRIVATE)

    fun saveUserInformation(userInformation: UserInformation){
        sharedPreferences.edit{
            putString(USER_NAME, userInformation.name)
            putLong(USER_DATE_OF_BIRTHDAY, userInformation.dateOfBirthday.time)
            putInt(USER_SEX, userInformation.sex.ordinal)
        }
    }

    fun getUserInformation(): UserInformation? {
        val userName = sharedPreferences.getString(USER_NAME, null)
        val userDateOfBirthday = Date(sharedPreferences.getLong(USER_DATE_OF_BIRTHDAY, 0))
        val userSex = UserSex.values().getOrNull(sharedPreferences.getInt(USER_SEX, -1))

        if(userName != null && userDateOfBirthday.time != 0L && userSex != null){
            return UserInformation(userName, userDateOfBirthday, userSex)
        }

        return null
    }

    companion object{
        private const val PREFERENCES_FILE_NAME = "HumbleDatingPreferences"
        private const val USER_NAME = "user_name"
        private const val USER_DATE_OF_BIRTHDAY = "user_date_of_birthday"
        private const val USER_SEX = "user_sex"

        private var singleton: PreferencesRepository? = null

        fun getInstance(context: Context): PreferencesRepository{
            val instance = singleton ?: PreferencesRepository(context)
            singleton = instance

            return instance
        }
    }
}