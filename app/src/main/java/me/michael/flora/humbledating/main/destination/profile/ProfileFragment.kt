package me.michael.flora.humbledating.main.destination.profile

import android.os.Bundle
import android.view.View
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputEditText
import me.michael.flora.humbledating.R
import me.michael.flora.humbledating.main.destination.BaseFragment
import me.michael.flora.humbledating.main.destination.profile.viewmodel.ProfileViewModel
import me.michael.flora.humbledating.main.viewmodel.MainViewModel

class ProfileFragment: BaseFragment(R.layout.fragment_profile) {
    private val sharedViewModel by activityViewModels<MainViewModel>()
    private val viewModel by viewModels<ProfileViewModel>()

    private var userNameText: TextInputEditText? = null
    private var saveButton: MaterialButton? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        findAllViews()

        initializeAllViews()
    }

    private fun findAllViews(){
        userNameText = view?.findViewById(R.id.profile_name)
        saveButton = view?.findViewById(R.id.profile_save_button)
    }

    private fun initializeAllViews(){
        initializeNameField()
        initializeSaveButton()
    }

    private fun initializeNameField(){
        userNameText?.setText(viewModel.name.value?.toString())

        userNameText?.addTextChangedListener { editable ->
            editable?.toString()?.let{ newName ->
                viewModel.updateName(newName)
            }
        }
    }

    private fun initializeSaveButton(){
        saveButton?.setOnClickListener {
            viewModel.saveUserInformation(sharedViewModel)
            sharedViewModel.goOnlineByUserAction()
            navigationController.popBackStack()
        }
    }
}