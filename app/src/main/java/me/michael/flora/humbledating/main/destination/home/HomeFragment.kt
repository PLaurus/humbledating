package me.michael.flora.humbledating.main.destination.home

import android.os.Bundle
import android.view.*
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import me.michael.flora.humbledating.R
import me.michael.flora.humbledating.main.destination.BaseFragment
import me.michael.flora.humbledating.main.destination.home.destination.nearbyusers.NearbyUsersFragmentDirections
import me.michael.flora.humbledating.main.viewmodel.MainViewModel

class HomeFragment: BaseFragment(R.layout.fragment_home) {
    private val sharedViewModel by activityViewModels<MainViewModel>()
    private val homeNavigationController by lazy { nestedFragmentContainer?.findNavController() }

    private var nestedFragmentContainer: View? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        findAllViews()
        observeNetworkState()
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.home_menu, menu)

        val networkStatusItem = menu.findItem(R.id.home_menu_item_change_network_state)

        sharedViewModel.isUserOnline.observe(this, Observer{ status ->
            when(status){
                true -> networkStatusItem.setIcon(R.drawable.ic_network_off_black_24dp)
                false -> networkStatusItem.setIcon(R.drawable.ic_network_on_black_24dp)
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId){
            R.id.home_menu_item_profile -> {
                navigationController.navigate(R.id.action_global_destination_profile)
                true
            }
            R.id.home_menu_item_change_network_state -> {
                when(sharedViewModel.isUserOnline.value){
                    true -> sharedViewModel.goOfflineByUserAction()
                    false -> sharedViewModel.goOnlineByUserAction()
                }
                true
            }
            else -> false
        }
    }

    private fun findAllViews(){
        nestedFragmentContainer = view?.findViewById(R.id.home_navigation_host_fragment)
    }

    private fun observeNetworkState(){
        sharedViewModel.areNearbyUseConditionsMet.observe(this as LifecycleOwner, Observer { nearbyUseConditionsMet ->
            if(!nearbyUseConditionsMet){
                if(homeNavigationController?.currentDestination?.id != R.id.destination_network_error){
                    homeNavigationController?.navigate(NearbyUsersFragmentDirections.actionDestinationNearbyUsersToDestinationNetworkError())
                }

                return@Observer
            }

            if(homeNavigationController?.currentDestination?.id != R.id.destination_nearby_users){
                homeNavigationController?.popBackStack()
            }
        })
    }

}