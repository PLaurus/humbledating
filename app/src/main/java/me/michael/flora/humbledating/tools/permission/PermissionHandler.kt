package me.michael.flora.humbledating.tools.permission

import android.Manifest
import android.app.Activity
import android.util.Log
import android.view.View
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.BaseMultiplePermissionsListener
import com.karumi.dexter.listener.multi.CompositeMultiplePermissionsListener
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.karumi.dexter.listener.multi.SnackbarOnAnyDeniedMultiplePermissionsListener
import com.karumi.dexter.listener.single.BasePermissionListener
import com.karumi.dexter.listener.single.CompositePermissionListener
import com.karumi.dexter.listener.single.PermissionListener
import com.karumi.dexter.listener.single.SnackbarOnDeniedPermissionListener
import me.michael.flora.humbledating.R

object PermissionHandler {
    private val logTag = this::class.java.simpleName

    private val nearbyPermissions = listOf(
        Manifest.permission.BLUETOOTH,
        Manifest.permission.BLUETOOTH_ADMIN,
        Manifest.permission.ACCESS_WIFI_STATE,
        Manifest.permission.CHANGE_WIFI_STATE,
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION
    )

    fun checkNearbyPermissions(
        activity: Activity,
        onPermissionsChecked: ((result: Boolean) -> Unit)? = null
    ){
        val deniedMessage = activity.resources.getString(R.string.permissions_handler_nearby_permission_denied_message)
        checkPermissions(activity, nearbyPermissions, deniedMessage, onPermissionsChecked)
    }

    private fun checkPermission(
        activity: Activity,
        permission: String,
        deniedMessage: String,
        onPermissionsChecked: ((result: Boolean) -> Unit)? = null
    ) {
        Dexter.withActivity(activity)
            .withPermission(permission)
            .withListener(getCompositePermissionListener(activity, deniedMessage, onPermissionsChecked))
            .onSameThread()
            .check()
    }

    private fun checkPermissions(
        activity: Activity,
        permissions: List<String>,
        deniedMessage: String,
        onPermissionsChecked: ((result: Boolean) -> Unit)? = null
    ){
        Dexter.withActivity(activity)
            .withPermissions(permissions)
            .withListener(getCompositeMultiplePermissionsListener(activity, deniedMessage, onPermissionsChecked))
            .onSameThread()
            .withErrorListener {
                Log.e(logTag, "Error occurred: $it")
            }
            .check()
    }

    private fun getCompositePermissionListener(
        activity: Activity,
        deniedMessage: String,
        onPermissionChecked: ((result: Boolean) -> Unit)? = null
    ): CompositePermissionListener{
        val listeners = mutableListOf<PermissionListener>().apply {
            add(getSnackBarPermissionListener(activity, deniedMessage))
            onPermissionChecked?.run{
                add(getBasePermissionListener(this))
            }
        }

        return CompositePermissionListener(listeners)
    }

    private fun getCompositeMultiplePermissionsListener(
        activity: Activity,
        deniedMessage: String,
        onPermissionsChecked: ((result: Boolean) -> Unit)? = null
    ): CompositeMultiplePermissionsListener{
        val listeners = mutableListOf<MultiplePermissionsListener>().apply {
            add(getSnackBarMultiplePermissionsListener(activity, deniedMessage))
            onPermissionsChecked?.run{
                add(getBaseMultiplePermissionsListener(this))
            }
        }

        return CompositeMultiplePermissionsListener(listeners)
    }

    private fun getBasePermissionListener(
        onPermissionChecked: (result: Boolean) -> Unit
    ): BasePermissionListener {
        return object: BasePermissionListener(){
            override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                onPermissionChecked.invoke(true)
            }

            override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                onPermissionChecked.invoke(false)
            }
        }
    }

    private fun getBaseMultiplePermissionsListener(
        onPermissionsChecked: (result: Boolean) -> Unit
    ): BaseMultiplePermissionsListener {
        return object: BaseMultiplePermissionsListener(){
            override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                if(report?.areAllPermissionsGranted() == true){
                    onPermissionsChecked.invoke(true)
                    return
                }

                onPermissionsChecked.invoke(false)
            }

            override fun onPermissionRationaleShouldBeShown(
                permissions: MutableList<PermissionRequest>?,
                token: PermissionToken?
            ) {
                token?.continuePermissionRequest()
            }
        }
    }

    private fun getSnackBarPermissionListener(
        activity: Activity,
        deniedMessage: String
    ): SnackbarOnDeniedPermissionListener{
        val parentView = activity.findViewById<View>(android.R.id.content)
        val settingsButtonTitle = activity.resources.getString(R.string.permissions_handler_settings_button)

        return SnackbarOnDeniedPermissionListener.Builder
            .with(parentView, deniedMessage)
            .withOpenSettingsButton(settingsButtonTitle)
            .build()
    }

    private fun getSnackBarMultiplePermissionsListener(
        activity: Activity,
        deniedMessage: String
    ): SnackbarOnAnyDeniedMultiplePermissionsListener{
        val parentView = activity.findViewById<View>(android.R.id.content)
        val settingsButtonTitle = activity.resources.getString(R.string.permissions_handler_settings_button)

        return SnackbarOnAnyDeniedMultiplePermissionsListener.Builder
            .with(parentView, deniedMessage)
            .withOpenSettingsButton(settingsButtonTitle)
            .build()
    }
}