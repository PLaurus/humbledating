package me.michael.flora.humbledating.main.destination.chat.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import me.michael.flora.humbledating.main.viewmodel.MainViewModel
import me.michael.flora.p2pcommunication.entity.Message
import me.michael.flora.p2pcommunication.entity.User
import java.util.*

class ChatViewModel(application: Application): AndroidViewModel(application) {

    private lateinit var user: User
    private var messageToSendText: String = ""
    private val mutableNewSentMessage = MutableLiveData<Message>()

    val newReceivedMessage: LiveData<Message> by lazy { user.communicationChannel.receivedMessage }
    val newSentMessage: LiveData<Message>  = mutableNewSentMessage

    val userName: String
        get() = user.userInformation.name

    fun initialize(user: User){
        this.user = user
    }

    fun updateMessageText(newText: String){
        messageToSendText = newText
    }

    fun sendMessage(sharedViewModel: MainViewModel){
        val message = Message(sharedViewModel.userInformation.value?.name ?: "" ,Date(), messageToSendText)
        user.communicationChannel.sendMessage(message)
        mutableNewSentMessage.value = message
    }

}