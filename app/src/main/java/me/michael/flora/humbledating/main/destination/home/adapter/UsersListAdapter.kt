package me.michael.flora.humbledating.main.destination.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.navigation.NavController
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.card.MaterialCardView
import me.michael.flora.humbledating.R
import me.michael.flora.humbledating.main.destination.home.HomeFragmentDirections
import me.michael.flora.humbledating.main.viewmodel.MainViewModel
import me.michael.flora.p2pcommunication.entity.User
import me.michael.flora.p2pcommunication.entity.UserSex

class UsersListAdapter(private val sharedViewModel: MainViewModel, private val navigationController: NavController?) : RecyclerView.Adapter<UsersListAdapter.UserViewHolder>(){

    private var users = mutableListOf<User>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        return UserViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_user, parent, false) as MaterialCardView
        )
    }

    override fun getItemCount(): Int = users.size

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        holder.apply {
            val user = users[position]

            userName.text = user.userInformation.name
            userDateOfBirthday.text = user.userInformation.dateOfBirthday.toString()
            userSex.text = when(user.userInformation.sex){
                UserSex.Male -> "Male"
                UserSex.Fermale -> "Female"
            }

            setOnClickListener {
                val action = HomeFragmentDirections.actionDestinationHomeToDestinationChat(position)
                navigationController?.navigate(action)
            }
        }
    }

    fun setNewUsers(newUsers: List<User>){
        users = newUsers.toMutableList()
        notifyDataSetChanged()
    }

    class UserViewHolder(private val userContainer: MaterialCardView): RecyclerView.ViewHolder(userContainer){
        val context = userContainer.context

        val userAvatar: AppCompatImageView by lazy { userContainer.findViewById<AppCompatImageView>(R.id.item_user_avatar) }
        val userName: AppCompatTextView by lazy { userContainer.findViewById<AppCompatTextView>(R.id.item_user_name) }
        val userDateOfBirthday: AppCompatTextView by lazy { userContainer.findViewById<AppCompatTextView>(R.id.item_user_date_of_birthday) }
        val userSex: AppCompatTextView by lazy { userContainer.findViewById<AppCompatTextView>(R.id.item_user_sex) }

        fun setOnClickListener(onClickListener: (MaterialCardView) -> Unit){
            userContainer.setOnClickListener {
                onClickListener(userContainer)
            }
        }
    }
}