package me.michael.flora.humbledating.entities.networkservice

import android.net.wifi.p2p.WifiP2pDevice

open class WifiP2pService(
    val device: WifiP2pDevice?,
    val instanceName: String?,
    val serviceRegistrationType: String?
)