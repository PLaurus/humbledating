package me.michael.flora.humbledating.tools.googleplay

import android.app.Activity
import android.content.DialogInterface
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability

object GooglePlayServicesManager {
    private val googleApiAvailability = GoogleApiAvailability.getInstance()

    fun checkGooglePlayServicesAvailability(activity: Activity, handleErrorRequestCode: Int, onCancelListener: ((dialog: DialogInterface) -> Unit)? = null): Boolean{

        return when(val checkResult = googleApiAvailability.isGooglePlayServicesAvailable(activity)){
            ConnectionResult.SUCCESS -> true
            else -> {
                googleApiAvailability.getErrorDialog(activity, checkResult, handleErrorRequestCode, onCancelListener)
                false
            }
        }
    }
}