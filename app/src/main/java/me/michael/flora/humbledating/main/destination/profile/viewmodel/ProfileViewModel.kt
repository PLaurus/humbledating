package me.michael.flora.humbledating.main.destination.profile.viewmodel

import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import me.michael.flora.humbledating.main.data.PreferencesRepository
import me.michael.flora.humbledating.main.viewmodel.MainViewModel
import me.michael.flora.p2pcommunication.entity.UserInformation
import me.michael.flora.p2pcommunication.entity.UserSex
import java.util.*

class ProfileViewModel(application: Application): AndroidViewModel(application) {

    private val applicationContext: Context
        get() = getApplication()

    private val preferencesRepository = PreferencesRepository.getInstance(applicationContext)

    private val mutableName = MutableLiveData<String>()

    val name: LiveData<String> = mutableName
    var dateOfBirthday = Date()
    var sex = UserSex.Male

    init{
        mutableName.value = preferencesRepository.getUserInformation()?.name
    }

    fun updateName(name: String){
        mutableName.value = name
    }

    fun saveUserInformation(sharedViewModel: MainViewModel){
        mutableName.value?.let{ nameNotNull ->
            val userInformation = UserInformation(nameNotNull, dateOfBirthday, sex)
            preferencesRepository.saveUserInformation(userInformation)
            sharedViewModel.updateUserInformation(userInformation)
        }
    }

}