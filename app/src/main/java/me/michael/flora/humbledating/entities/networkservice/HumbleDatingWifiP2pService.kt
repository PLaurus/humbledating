package me.michael.flora.humbledating.entities.networkservice

class HumbleDatingWifiP2pService(
    wifiP2pService: WifiP2pService,
    val host: String,
    val port: Int
): WifiP2pService(wifiP2pService.device, wifiP2pService.instanceName, wifiP2pService.serviceRegistrationType)