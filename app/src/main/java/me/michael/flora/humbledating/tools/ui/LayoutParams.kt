package me.michael.flora.humbledating.tools.ui

import android.content.Context
import android.view.ViewGroup
import androidx.annotation.Dimension
import androidx.core.view.updateMargins

fun <T: ViewGroup.MarginLayoutParams> T.setMarginStartDp(context: Context, @Dimension(unit = Dimension.DP) margin: Int){
    marginStart = margin.dpToPixel(context)
}

fun <T: ViewGroup.MarginLayoutParams> T.setMarginEndDp(context: Context, @Dimension(unit = Dimension.DP) margin: Int){
    marginEnd = margin.dpToPixel(context)
}

fun <T: ViewGroup.MarginLayoutParams> T.setMarginTopDp(context: Context, @Dimension(unit = Dimension.DP) margin: Int){
    val pixelMargin = margin.dpToPixel(context)
    updateMargins(top = pixelMargin)
}

fun <T: ViewGroup.MarginLayoutParams> T.setMarginBottomDp(context: Context, @Dimension(unit = Dimension.DP) margin: Int){
    val pixelMargin = margin.dpToPixel(context)
    updateMargins(bottom = pixelMargin)
}