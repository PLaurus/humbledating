package me.michael.flora.humbledating.main.destination.home.destination.networkerror

import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.fragment.app.activityViewModels
import com.google.android.material.button.MaterialButton
import me.michael.flora.humbledating.R
import me.michael.flora.humbledating.main.MainActivity
import me.michael.flora.humbledating.main.destination.BaseFragment
import me.michael.flora.humbledating.main.viewmodel.MainViewModel

class NetworkErrorFragment: BaseFragment(R.layout.fragment_network_error) {
    private val mainViewModel by activityViewModels<MainViewModel>()

    private var offlineImage: AppCompatImageView? = null
    private var errorText: AppCompatTextView? = null
    private var enterNetworkButton: MaterialButton? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        findAllViews()
        initializeAllViews()
        super.onViewCreated(view, savedInstanceState)
    }

    private fun findAllViews(){
        view?.run{
            offlineImage = findViewById(R.id.network_error_offline_icon)
            errorText = findViewById(R.id.network_error_message)
            enterNetworkButton = findViewById(R.id.network_error_enter_network_button)
        }
    }

    private fun initializeAllViews(){
        initializeEnterNetworkButton()
    }

    private fun initializeEnterNetworkButton(){
        enterNetworkButton?.setOnClickListener {
            val mainActivity = (activity as? MainActivity)

            mainActivity?.useNearbyCommunication()
        }
    }
}