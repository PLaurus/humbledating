package me.michael.flora.humbledating.main

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import me.michael.flora.humbledating.MainNavigationGraphDirections
import me.michael.flora.humbledating.R
import me.michael.flora.humbledating.main.service.CommunicationService
import me.michael.flora.humbledating.main.viewmodel.MainViewModel
import me.michael.flora.humbledating.tools.googleplay.GooglePlayServicesManager
import me.michael.flora.humbledating.tools.permission.PermissionHandler

class MainActivity: AppCompatActivity(R.layout.activity_main) {

    private val viewModel by viewModels<MainViewModel>()
    val navigationController by lazy { findNavController(R.id.main_navigation_host_fragment) }

    private lateinit var toolbar: Toolbar

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)

        intent?.run {
            handleNewMessageNotificationIntent(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        intent?.run {
            handleNewMessageNotificationIntent(intent)
        }

        findAllViews()
        initializeToolbar()
        initializeToastMessages()
    }

    override fun onResume() {
        super.onResume()

        useNearbyCommunication()
    }

    override fun onPause() {
        super.onPause()
        if(viewModel.isCommunicationServiceAvailable.value == true){
            unbindService(viewModel.communicationServiceBindingCallback)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when(requestCode){
            HANDLE_GOOGLE_PLAY_SERVICES_AVAILABILITY_ERROR_REQUEST_CODE ->{
                if(resultCode == Activity.RESULT_OK){
                    checkGooglePlayServicesAvailability()
                }else{
                    finish()
                }
            }
            else -> super.onActivityResult(requestCode, resultCode, data)
        }
    }

    fun useNearbyCommunication(){
        checkNearbyUseConditions{ result ->
            if(result){
                onNearbyUseConditionsMet()
            } else {
                onNearbyUseConditionsNotMet()
            }
        }
    }

    private fun checkNearbyUseConditions(onResult: (Boolean) -> Unit){
        checkGooglePlayServicesAvailability()

        if(viewModel.isGooglePlayServicesAvailable.value == true){
            PermissionHandler.checkNearbyPermissions(this){ result ->
                viewModel.setNearbyUseConditionsMet(result)
                onResult(result)
            }
        } else{
            onResult(false)
        }
    }

    private fun onNearbyUseConditionsMet(){
        viewModel.goOnlineBySystemAction()
        bindService(Intent(applicationContext, CommunicationService::class.java), viewModel.communicationServiceBindingCallback, 0)
    }

    private fun onNearbyUseConditionsNotMet(){
        if(viewModel.isCommunicationServiceAvailable.value == true){
            unbindService(viewModel.communicationServiceBindingCallback)
        }
        viewModel.goOfflineBySystemAction()
    }

    private fun checkGooglePlayServicesAvailability(){
        val isGooglePlayServicesAvailable = GooglePlayServicesManager.checkGooglePlayServicesAvailability(
            this,
            HANDLE_GOOGLE_PLAY_SERVICES_AVAILABILITY_ERROR_REQUEST_CODE){
            finish()
        }

        viewModel.changeGooglePlayServicesAvailabilityState(isGooglePlayServicesAvailable)
    }

    private fun findAllViews(){
        toolbar = findViewById(R.id.toolbar)
    }

    private fun initializeToolbar(){
        setSupportActionBar(toolbar)
        toolbar.setupWithNavController(navigationController)
    }

    private fun initializeToastMessages(){
        viewModel.systemMessage.observe(this, Observer { systemMessage ->
            Toast.makeText(this@MainActivity, systemMessage, Toast.LENGTH_LONG).show()
        })
    }

    private fun handleNewMessageNotificationIntent(intent: Intent){

        if(navigationController.currentDestination?.id == R.id.destination_chat){
            return
        }

        val actionId = intent.getIntExtra(CommunicationService.NAVIGATION_ACTION_EXTRA_KEY, -1)

        if(actionId == R.id.action_global_destination_chat){
            val userIndex = intent.getIntExtra(CommunicationService.USER_INDEX_EXTRA_KEY, -1)

            if(userIndex < 0){
                return
            }

            viewModel.isCommunicationServiceAvailable.observe(
                this,
                object: Observer<Boolean> {
                    override fun onChanged(serviceAvailable: Boolean?) {
                        if(serviceAvailable == true){
                            val action = MainNavigationGraphDirections.actionGlobalDestinationChat(userIndex)
                            navigationController.navigate(action)
                            viewModel.isCommunicationServiceAvailable.removeObserver(this)
                        }
                    }

                }
            )

        }
    }

    companion object{
        private const val HANDLE_GOOGLE_PLAY_SERVICES_AVAILABILITY_ERROR_REQUEST_CODE = 0
    }

}