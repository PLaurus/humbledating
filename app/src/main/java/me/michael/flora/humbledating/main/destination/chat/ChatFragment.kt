package me.michael.flora.humbledating.main.destination.chat

import android.os.Bundle
import android.view.View
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputEditText
import me.michael.flora.humbledating.R
import me.michael.flora.humbledating.main.destination.BaseFragment
import me.michael.flora.humbledating.main.destination.chat.adapter.MessagesListAdapter
import me.michael.flora.humbledating.main.destination.chat.viewmodel.ChatViewModel
import me.michael.flora.humbledating.main.viewmodel.MainViewModel

class ChatFragment: BaseFragment(R.layout.fragment_chat) {
    private val viewModel by viewModels<ChatViewModel>()
    private val sharedViewModel by activityViewModels<MainViewModel>()
    private val passedArguments by navArgs<ChatFragmentArgs>()

    private var chatRecyclerView: RecyclerView? = null
    private var messageField: TextInputEditText? = null
    private var sendButton: MaterialButton? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val userIndex = passedArguments.argumentUserIndex
        val user = sharedViewModel.availableUsers?.value?.getOrNull(userIndex)

        user?.run{
            viewModel.initialize(this)
            findAllViews()
            initializeAllViews()
        } ?: navigationController.popBackStack()
    }

    private fun findAllViews(){
        view?.run {
            chatRecyclerView = findViewById(R.id.chat)
            messageField = findViewById(R.id.message)
            sendButton = findViewById(R.id.send_message)
        }
    }

    private fun initializeAllViews(){
        initializeMessageField()
        initializeChatRecyclerView()
        initializeSendButton()
    }

    private fun initializeMessageField(){
        messageField?.addTextChangedListener { editable ->
            editable?.toString()?.let { newMessageText ->
                viewModel.updateMessageText(newMessageText)
            }
        }
    }

    private fun initializeChatRecyclerView(){
        chatRecyclerView?.setHasFixedSize(true)
        chatRecyclerView?.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, true)
        val messagesListAdapter = MessagesListAdapter()
        chatRecyclerView?.adapter = messagesListAdapter

        viewModel.newSentMessage.observe(
            this as LifecycleOwner,
            Observer{ newMessage ->
                messagesListAdapter.addNewSentMessage(newMessage)
            }
        )

        viewModel.newReceivedMessage.observe(
            this as LifecycleOwner,
            Observer{ newMessage ->
                messagesListAdapter.addNewReceivedMessage(newMessage)
            }
        )
    }

    private fun initializeSendButton(){
        sendButton?.setOnClickListener {
            viewModel.sendMessage(sharedViewModel)
            messageField?.text?.clear()
        }
    }
}