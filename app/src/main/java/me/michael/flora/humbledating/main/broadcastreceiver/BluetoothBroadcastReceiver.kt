package me.michael.flora.humbledating.main.broadcastreceiver

import android.bluetooth.BluetoothAdapter
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class BluetoothBroadcastReceiver private constructor(): BroadcastReceiver() {

    private var onBluetoothTurnedOff: (() -> Unit)? = null
    private var onBluetoothTurningOff: (() -> Unit)? = null
    private var onBluetoothTurnedOn: (() -> Unit)? = null
    private var onBluetoothTurningOn: (() -> Unit)? = null

    override fun onReceive(context: Context?, intent: Intent?) {
        val action = intent?.action

        if(action == BluetoothAdapter.ACTION_CONNECTION_STATE_CHANGED){
            val state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1)

            when(state){
                BluetoothAdapter.STATE_OFF -> onBluetoothTurnedOff?.invoke()
                BluetoothAdapter.STATE_TURNING_OFF -> onBluetoothTurningOff?.invoke()
                BluetoothAdapter.STATE_ON -> onBluetoothTurnedOn?.invoke()
                BluetoothAdapter.STATE_TURNING_ON -> onBluetoothTurningOn?.invoke()
            }
        }
    }

    companion object{
        fun newBuilder(): Builder = BluetoothBroadcastReceiver().Builder()
    }

    inner class Builder{
        fun addOnBluetoothTurnedOffListener(listener: () -> Unit): Builder{
            onBluetoothTurnedOff = listener
            return this
        }

        fun addOnBluetoothTurningOffListener(listener: () -> Unit): Builder{
            onBluetoothTurningOff = listener
            return this
        }

        fun addOnBluetoothTurnedOnListener(listener: () -> Unit): Builder{
            onBluetoothTurnedOn = listener
            return this
        }

        fun addOnBluetoothTurningOnListener(listener: () -> Unit): Builder{
            onBluetoothTurningOff = listener
            return this
        }

        fun build(): BluetoothBroadcastReceiver = this@BluetoothBroadcastReceiver
    }
}