package me.michael.flora.humbledating.main.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import me.michael.flora.humbledating.main.service.CommunicationService

@Suppress("UNCHECKED_CAST")
class MainViewModelFactory(
    private val application: Application,
    private val communicationService: CommunicationService
): ViewModelProvider.AndroidViewModelFactory(application)
{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when(modelClass){
            MainViewModel::class.java -> {
                MainViewModel(application) as T
            }
            else -> super.create(modelClass)
        }
    }
}