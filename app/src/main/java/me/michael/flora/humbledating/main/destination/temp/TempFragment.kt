package me.michael.flora.humbledating.main.destination.temp

import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import com.google.android.material.button.MaterialButton
import me.michael.flora.humbledating.R
import me.michael.flora.humbledating.main.destination.BaseFragment
import me.michael.flora.humbledating.main.viewmodel.MainViewModel

class TempFragment: BaseFragment(R.layout.fragment_temp) {
    private val sharedViewModel by activityViewModels<MainViewModel>()
    private var disconnectButton: MaterialButton? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        findAllViews()

        initializeViews()

        super.onViewCreated(view, savedInstanceState)
    }

    private fun findAllViews(){
        disconnectButton = view?.findViewById(R.id.disconnect_button)
    }

    private fun initializeViews(){
    }

}