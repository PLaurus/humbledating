package me.michael.flora.humbledating.main.destination.chat.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.AppCompatToggleButton
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.recyclerview.widget.RecyclerView
import me.michael.flora.humbledating.R
import me.michael.flora.humbledating.entities.chat.ChatMessage
import me.michael.flora.humbledating.tools.ui.dpToPixel
import me.michael.flora.p2pcommunication.entity.Message
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class MessagesListAdapter: RecyclerView.Adapter<MessagesListAdapter.MessageViewHolder>() {

    private val dateFormatter = SimpleDateFormat.getTimeInstance(DateFormat.SHORT, Locale.getDefault())
    private val chatMessages = mutableListOf<ChatMessage>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageViewHolder {
        return MessageViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.chat_message, parent, false) as ConstraintLayout
        )
    }

    override fun getItemCount(): Int = chatMessages.size

    override fun onBindViewHolder(holder: MessageViewHolder, position: Int) {
        with(holder){
            val chatMessage = chatMessages[position]
            val messageContent = chatMessage.message.content
            val messageTime = dateFormatter.format(chatMessage.message.date)

            messageText.text = messageContent
            timeText.text = messageTime

            if(chatMessages[position].isSent){
                setOutgoingMessageStyle()
            } else{
                setIncomingMessageStyle()
            }

        }
    }

    fun addNewSentMessage(message: Message){
        addNewChatMessage(message, isMessageSent = true)
    }

    fun addNewReceivedMessage(message: Message){
        addNewChatMessage(message, isMessageSent = false)
    }

    private fun addNewChatMessage(message: Message, isMessageSent: Boolean){
        val chatMessage = ChatMessage(message, isMessageSent)
        chatMessages.add(0, chatMessage)
        notifyDataSetChanged()
    }

    class MessageViewHolder(private val container: ConstraintLayout): RecyclerView.ViewHolder(container){
        private val messageCheckerButton: AppCompatToggleButton = container.findViewById(R.id.received_message_checker_button)
        private val messageContainer: ConstraintLayout = container.findViewById(R.id.received_message_container)
        val messageText: AppCompatTextView = container.findViewById(R.id.received_message_text)
        val timeText: AppCompatTextView = container.findViewById(R.id.received_message_time)

        fun setIncomingMessageStyle(){
            val newContainerConstraintSet = ConstraintSet().apply {
                clone(container)
                setHorizontalBias(R.id.received_message_container, 0.0f)
                setMargin(R.id.received_message_container, ConstraintSet.START, 0)
                setMargin(R.id.received_message_container, ConstraintSet.END, MAX_WIDTH_MARGIN_DP.dpToPixel(container.context))
            }
            container.setConstraintSet(newContainerConstraintSet)
            messageContainer.setBackgroundResource(R.drawable.message_bubble_in)
        }

        fun setOutgoingMessageStyle(){
            val newContainerConstraintSet = ConstraintSet().apply {
                clone(container)
                setHorizontalBias(R.id.received_message_container, 1.0f)
                setMargin(R.id.received_message_container, ConstraintSet.END, 0)
                setMargin(R.id.received_message_container, ConstraintSet.START, MAX_WIDTH_MARGIN_DP.dpToPixel(container.context))
            }
            container.setConstraintSet(newContainerConstraintSet)
            messageContainer.setBackgroundResource(R.drawable.message_bubble_out)
        }

        companion object {
            private const val MAX_WIDTH_MARGIN_DP = 80
        }
    }
}