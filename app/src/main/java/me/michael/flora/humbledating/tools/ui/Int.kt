package me.michael.flora.humbledating.tools.ui

import android.content.Context
import android.util.TypedValue

fun Int.dpToPixel(context: Context): Int{
    val displayMetrics = context.resources.displayMetrics

    return TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP,
        this.toFloat(),
        displayMetrics
    ).toInt()
}