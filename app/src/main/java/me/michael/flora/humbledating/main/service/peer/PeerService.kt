package me.michael.flora.humbledating.main.service.peer

import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Binder
import android.os.IBinder
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.*
import java.io.IOException
import java.net.InetSocketAddress
import java.net.ServerSocket
import java.net.Socket

class PeerService: Service() {
    private val logTag = this::class.java.simpleName
    private val serviceJob by lazy { Job() }
    private val serviceScope: CoroutineScope
        get() = MainScope() + serviceJob

    private var serverSocket: ServerSocket? = null
    private var isServerStarted = false
    private val connectedClients = mutableListOf<Socket>()
    private val mutableMessages = MutableLiveData<List<Pair<Socket?, String>>>()

    val address = MutableLiveData<InetSocketAddress>()

    val messages: LiveData<List<Pair<Socket?, String>>> = mutableMessages

    override fun onCreate() {
        super.onCreate()
        startServer()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onBind(intent: Intent?): IBinder? {
        return ServerServiceBinder()
    }

    override fun onDestroy() {
        super.onDestroy()
        stopServer()
        serviceJob.cancel()
    }

    fun startServer(){

        if(isServerStarted)
            return

        try{
            serverSocket = ServerSocket(0)
            val host = serverSocket?.inetAddress?.hostAddress
            val port = serverSocket?.localPort
            host?.let{ hostNotNull ->
                port?.let{ portNotNull ->
                    address.value = InetSocketAddress(hostNotNull, portNotNull)
                }
            }

            Log.d(logTag, "Server: Socket opened")

            isServerStarted = true

            listenForNewClients()

            listenForDataSentByClients()
        } catch(ex: IOException){
            Log.e(logTag, "Server error occurred: ${ex.message}")
        }
    }

    fun stopServer(){
        try{
            connectedClients.forEach { client ->
                client.close()
                connectedClients.remove(client)
            }

            serverSocket?.close()

            Log.d(logTag, "Server: Socket closed")

            isServerStarted = false
        } catch(ex: IOException){
            Log.e(logTag, "Server error occured: ${ex.message}")
        }
    }

    fun sendDataToClients(data: String, sender: Socket? = null) = serviceScope.launch(Dispatchers.Default){
        serverSocket?.run {
            if(isServerStarted) {
                launch(Dispatchers.Main) {
                    mutableMessages.value = mutableMessages.value?.toMutableList()?.apply { add(Pair(sender, data)) }
                }

                val filteredClients = if(sender != null) connectedClients.filter { it != sender } else connectedClients

                filteredClients.forEach { client ->
                    launch(Dispatchers.IO) {
                        val bufferedWriter = client.getOutputStream().bufferedWriter()
                        bufferedWriter.write(data)
                        bufferedWriter.flush()
                        bufferedWriter.close()
                    }
                }
            }
        }
    }

    private fun listenForNewClients(onClientConnected: ((client: Socket, coroutineScope: CoroutineScope) -> Unit)? = null) = serviceScope.launch(Dispatchers.IO){
        serverSocket?.run{
            while(isServerStarted){
                try{
                    val newClient = accept()
                    connectedClients.add(newClient)
                    onClientConnected?.invoke(newClient, this@launch)
                    Log.e(logTag, "New client connected: ${newClient.remoteSocketAddress}")
                } catch(ex: IOException){
                    Log.e(logTag, "Server failed to accept new client")
                }

            }
        }
    }

    private fun listenForDataSentByClients() = serviceScope.launch(Dispatchers.Default) {
        serverSocket?.run {
            while (isServerStarted) {
                delay(DATA_LISTENING_DELAY_TIME_MILLIS)
                try {
                    connectedClients.forEach { client ->
                        launch(Dispatchers.IO) {
                            val bufferedReader = client.getInputStream().bufferedReader()
                            val receivedData = bufferedReader.readText()
                            sendDataToClients(receivedData, client)
                        }
                    }
                } catch(ex: IOException){
                    Log.e(logTag, "the socket is closed, the socket is not connected, or the socket input has been shutdown using shutdownInput()")
                }
            }
        }
    }


    //====== Client =========
//    private val serverUsersConnections = mutableMapOf<User, Socket>()
//
//    fun connectToServer(user: User){
//        val socket = Socket()
//
//        try{
//            Log.d(logTag, "Opening client socket")
//            socket.bind(null)
//
//            val serverAddress = InetSocketAddress(user.host, user.port)
//
//            socket.connect(serverAddress, CONNECTION_TIMEOUT_MILLIS)
//
//            serverUsersConnections[user] = socket
//
//            Log.d(logTag, "The connection to remote peer is successfully established")
//
//        } catch (socketTimeoutException: SocketTimeoutException){
//            val timeoutSeconds = CONNECTION_TIMEOUT_MILLIS / 1000
//            Log.e(logTag, "The connection was not established during $timeoutSeconds seconds")
//        } catch(ex: Exception){
//            Log.e(logTag, "Error occurred during connection: ${ex.message}")
//            ex.printStackTrace()
//        } finally {
//            if(socket.isConnected){
//                try {
//                    socket.close()
//                } catch(ex: IOException){
//                    Log.e(logTag, "Failed to close connection")
//                    ex.printStackTrace()
//                }
//            }
//        }
//    }
//
//    fun disconnectFromServer(user: User? = null){
//        if(user == null){
//            serverUsersConnections.keys.forEach { connectedUser: User ->
//                val socket = serverUsersConnections[connectedUser]
//
//                if(socket?.isConnected == true){
//                    try {
//                        socket.close()
//                    } catch(ex: IOException){
//                        Log.e(logTag, "Failed to close connection")
//                        ex.printStackTrace()
//                    }
//                }
//
//                serverUsersConnections.remove(connectedUser)
//            }
//        }
//
//        val socket = serverUsersConnections[user]
//
//        if(socket?.isConnected == true){
//            try {
//                socket.close()
//            } catch(ex: IOException){
//                Log.e(logTag, "Failed to close connection")
//                ex.printStackTrace()
//            }
//        }
//    }
//
//    fun sendDataToServer(user: User, message: String){
//        try{
//            val socketOutputStream = serverUsersConnections[user]?.getOutputStream()
//
//            val bufferedWriter = socketOutputStream?.bufferedWriter()
//            bufferedWriter?.write(message)
//            bufferedWriter?.flush()
//            bufferedWriter?.close()
//
//            Log.d(logTag, "Message was successfully sent")
//        } catch(ex: IOException){
//            Log.e(logTag, "Error occurred during connection: ${ex.message}")
//            ex.printStackTrace()
//        }
//    }

    inner class ServerServiceBinder: Binder(){
        fun getService(): PeerService = this@PeerService
    }

    companion object{
        private const val CONNECTION_TIMEOUT_MILLIS = 5000
        private const val DATA_LISTENING_DELAY_TIME_MILLIS = 500L

        private var service: Intent? = null

        fun startService(packageContext: Context){
            service = Intent(packageContext, PeerService::class.java)

            packageContext.startService(service)
        }

        fun stopService(packageContext: Context){
            service?.run {
                packageContext.stopService(this)
            }
        }

        fun bindService(packageContext: Context, serviceConnection: ServiceConnection){
            packageContext.bindService(service, serviceConnection, Context.BIND_AUTO_CREATE)
        }

        fun unbindService(packageContext: Context, serviceConnection: ServiceConnection){
            packageContext.unbindService(serviceConnection)
        }
    }
}