package me.michael.flora.wifidirect.service

import android.net.wifi.p2p.WifiP2pDevice
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceInfo

class WifiDirectService private constructor(
    val instanceName: String,
    val protocol: String,
    val transportLayer: String
){
    private val serviceType: String = "_$protocol._$transportLayer"
    private val mutableRecords: MutableMap<String, String> = mutableMapOf()

    val records: Map<String, String> = mutableRecords
    var srcDevice: WifiP2pDevice? = null
        private set

    fun toWifiP2pDnsSdServiceInfo(): WifiP2pDnsSdServiceInfo = WifiP2pDnsSdServiceInfo.newInstance(instanceName, serviceType, records)

    companion object{
        fun newBuilder(
            serviceInstanceName: String,
            protocol:String,
            transportLayer: String
        ): Builder = WifiDirectService(serviceInstanceName, protocol, transportLayer).Builder()
    }

    inner class Builder{
        fun addRecord(key: String, value: String): Builder{
            mutableRecords[key] = value
            return this
        }

        fun addSourceDevice(device: WifiP2pDevice): Builder{
            srcDevice = device
            return this
        }

        fun build(): WifiDirectService = this@WifiDirectService
    }
}