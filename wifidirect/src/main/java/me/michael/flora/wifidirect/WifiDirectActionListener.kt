package me.michael.flora.wifidirect

import android.net.wifi.p2p.WifiP2pManager

class WifiDirectActionListener(private val onResultListener: (result: WifiDirectOperationResult) -> Unit): WifiP2pManager.ActionListener {
    override fun onSuccess() {
        onResultListener(WifiDirectOperationResult.Success)
    }

    override fun onFailure(reason: Int) {
        val result = when (reason) {
            WifiP2pManager.BUSY -> {
                WifiDirectOperationResult.Busy
            }
            WifiP2pManager.P2P_UNSUPPORTED -> {
                WifiDirectOperationResult.P2pUnsupported
            }
            else -> WifiDirectOperationResult.Error
        }

        onResultListener(result)
    }

    enum class WifiDirectOperationResult{
        /**
         * Successfully started peers discovery
         */
        Success,

        /**
         * Indicates that the operation failed due to an internal error.
         */
        Error,

        /**
         * Indicates that the operation failed because the framework is busy and unable to service the request
         */
        Busy,

        /**
         * Indicates that the operation failed because p2p is unsupported on the device.
         */
        P2pUnsupported
    }
}