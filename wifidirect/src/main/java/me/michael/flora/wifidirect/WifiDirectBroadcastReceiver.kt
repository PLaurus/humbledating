package me.michael.flora.wifidirect

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.NetworkInfo
import android.net.wifi.p2p.WifiP2pDevice
import android.net.wifi.p2p.WifiP2pManager

class WifiDirectBroadcastReceiver: BroadcastReceiver() {
    private var onWifiDirectStateChanged: ((enabled: Boolean) -> Unit)? = null
    private var onPeersChanged: (() -> Unit)? = null
    private var onConnectionStateChanged: ((connected: Boolean) -> Unit)? = null
    private var onThisDeviceChanged: ((device: WifiP2pDevice) -> Unit)? = null

    override fun onReceive(context: Context?, intent: Intent?) {
        when(intent?.action){
            WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION -> {
                val state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1) == WifiP2pManager.WIFI_P2P_STATE_ENABLED
                onWifiDirectStateChanged?.invoke(state)
            }
            WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION -> {
                onPeersChanged?.invoke()
            }
            WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION -> {
                val networkInfo: NetworkInfo? = intent.getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO) as NetworkInfo
                val isConnected = networkInfo?.isConnected ?: false

                onConnectionStateChanged?.invoke(isConnected)
            }
            WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION -> {
                val device = intent.getParcelableExtra<WifiP2pDevice>(WifiP2pManager.EXTRA_WIFI_P2P_DEVICE)
                device?.let{
                    onThisDeviceChanged?.invoke(it)
                }
            }
        }
    }

    companion object{
        fun newBuilder(): Builder = WifiDirectBroadcastReceiver().Builder()

        val intentFilter = IntentFilter().apply {
            // Indicates a change in the Wi-Fi P2P status
            addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION)

            // Indicates a change in the list of available peers.
            addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION)

            // Indicates the state of Wi-Fi P2P connectivity has changed.
            addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION)

            // Indicates this device's details have changed.
            addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION)
        }
    }

    inner class Builder{

        fun addOnWifiDirectStateChangedListener(onWifiDirectStateChanged: (enabled: Boolean) -> Unit): Builder {
            this@WifiDirectBroadcastReceiver.onWifiDirectStateChanged = onWifiDirectStateChanged
            return this
        }

        fun addOnPeersChangedListener(onPeersChanged: () -> Unit): Builder {
            this@WifiDirectBroadcastReceiver.onPeersChanged = onPeersChanged
            return this
        }

        fun addOnConnectionStateChangedListener(onConnectionStateChanged: (connected: Boolean) -> Unit): Builder {
            this@WifiDirectBroadcastReceiver.onConnectionStateChanged = onConnectionStateChanged
            return this
        }

        fun addOnThisDeviceChangedListener(onThisDeviceChanged: (device: WifiP2pDevice) -> Unit): Builder {
            this@WifiDirectBroadcastReceiver.onThisDeviceChanged = onThisDeviceChanged
            return this
        }

        fun build(): WifiDirectBroadcastReceiver = this@WifiDirectBroadcastReceiver
    }
}