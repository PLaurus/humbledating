package me.michael.flora.wifidirect.service

import android.Manifest
import android.net.wifi.p2p.WifiP2pDevice
import android.net.wifi.p2p.WifiP2pManager
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceInfo
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceRequest
import androidx.annotation.RequiresPermission
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import me.michael.flora.wifidirect.WifiDirectActionListener
import java.util.*

internal class WifiDirectServiceManager private constructor(
    private val p2pManager: WifiP2pManager,
    private val p2pChannel: WifiP2pManager.Channel
) {

    private val startedLocalServices: MutableMap<WifiDirectService ,WifiP2pDnsSdServiceInfo> = mutableMapOf()
    private val availableRecords: MutableMap<String, Map<String, String>> = mutableMapOf()
    private val mutableAvailableRemoteServices = MutableLiveData<Map<String, WifiDirectService>>(emptyMap())
    private val mutableAvailableRemoteServicesWithRecords = MutableLiveData<Map<String, WifiDirectService>>(emptyMap())

    val availableRemoteServices: LiveData<Map<String, WifiDirectService>> = mutableAvailableRemoteServices
    val availableRemoteServicesWithRecords: LiveData<Map<String, WifiDirectService>> = mutableAvailableRemoteServicesWithRecords

    private val onWifiDirectServiceAvailable = { instanceName: String?, registrationType: String?, srcDevice: WifiP2pDevice? ->

        if(instanceName != null && registrationType != null && srcDevice != null){
            val registrationTypeBlocks = registrationType.trimStart('_').split("._")
            registrationTypeBlocks.getOrNull(0)?.let { protocol ->
                registrationTypeBlocks.getOrNull(1)?.let { transportLayer ->
                    val serviceBuilder = WifiDirectService.newBuilder(instanceName, protocol, transportLayer)
                        .addSourceDevice(srcDevice)

                    val recordsForService = availableRecords[instanceName.toLowerCase(Locale.ENGLISH)]

                    recordsForService?.forEach{ entry ->
                        serviceBuilder.addRecord(entry.key, entry.value)
                    }

                    val service = serviceBuilder.build()

                    addNewRemoteService(service)

                    recordsForService?.run {
                        addNewRemoteServiceWithRecords(service)
                    }
                }
            }
        }

        Unit
    }

    private val onDnsSdTxtRecordAvailable = { fullDomainName: String?, txtRecordMap: MutableMap<String, String>?, srcDevice: WifiP2pDevice? ->

        if(fullDomainName != null && txtRecordMap != null && srcDevice != null){
            fullDomainName.trimStart('_').split("._").getOrNull(0)?.let { instanceName ->
                availableRecords[instanceName] = txtRecordMap
//                mutableAvailableRemoteServices.value?.get(instanceName)?.let{ service ->
//                    val serviceBuilder = service.Builder()
//
//                    txtRecordMap.forEach { entry ->
//                        serviceBuilder.addRecord(entry.key, entry.value)
//                    }
//
//                    serviceBuilder.build()
//
//                    addNewRemoteServiceWithRecords(service)
//                }
            }
        }

        Unit
    }

    /**
     * Register a local service for service discovery. If a local service is registered,
     * the framework automatically responds to a service discovery request from a peer.
     */
    @RequiresPermission(Manifest.permission.ACCESS_FINE_LOCATION)
    fun addLocalService(service: WifiDirectService, onResultListener: ((result: WifiDirectActionListener.WifiDirectOperationResult) -> Unit)? = null){
        val serviceInformation = service.toWifiP2pDnsSdServiceInfo()

        p2pManager.addLocalService(
            p2pChannel,
            serviceInformation,
            WifiDirectActionListener{ result ->
                if(result == WifiDirectActionListener.WifiDirectOperationResult.Success){
                    startedLocalServices[service] = serviceInformation
                }

                onResultListener?.invoke(result)
            }
        )
    }

    /**
     * Remove a registered local service added with [addLocalService]
     */
    fun removeLocalService(service: WifiDirectService, onResultListener: ((result: WifiDirectActionListener.WifiDirectOperationResult) -> Unit)? = null){
        val serviceInformation = startedLocalServices[service] ?: return

        p2pManager.removeLocalService(
            p2pChannel,
            serviceInformation,
            WifiDirectActionListener{ result ->
                if(result == WifiDirectActionListener.WifiDirectOperationResult.Success){
                    startedLocalServices.remove(service)
                }

                onResultListener?.invoke(result)
            }
        )
    }

    /**
     * Remove all registered local services from service discovery.
     */
    fun removeAllLocalServices(onResultListener: ((result: WifiDirectActionListener.WifiDirectOperationResult) -> Unit)? = null){
        p2pManager.clearLocalServices(p2pChannel, onResultListener?.let { WifiDirectActionListener(it)})
    }

    /**
     * Initiate service discovery. A discovery process involves scanning for requested services
     * for the purpose of establishing a connection to a peer that supports an available service.
     */
    fun startServicesDiscovery(onResultListener: ((result: WifiDirectActionListener.WifiDirectOperationResult) -> Unit)? = null){
        requestServiceDiscoveryFeature{ requestFeatureResult ->
            if(requestFeatureResult == WifiDirectActionListener.WifiDirectOperationResult.Success){

                p2pManager.setDnsSdResponseListeners(
                    p2pChannel,
                    onWifiDirectServiceAvailable,
                    onDnsSdTxtRecordAvailable
                )

                startServiceDiscoveryFeature(onResultListener)
            } else {
                onResultListener?.invoke(requestFeatureResult)
            }
        }
    }

    /**
     * Cancel any ongoing service discovery.
     */
    fun stopServicesDiscovery(onResultListener: ((result: WifiDirectActionListener.WifiDirectOperationResult) -> Unit)? = null){
        removeServiceDiscoveryFeatureRequests(onResultListener)
    }

    private fun addNewRemoteService(service: WifiDirectService){
        addNewService(service, mutableAvailableRemoteServices)
    }

    private fun addNewRemoteServiceWithRecords(service: WifiDirectService){
        addNewService(service, mutableAvailableRemoteServicesWithRecords)
    }

    private fun addNewService(service: WifiDirectService, to: MutableLiveData<Map<String, WifiDirectService>>){
        val newMap = to.value?.toMutableMap() ?: mutableMapOf()

        newMap[service.instanceName] = service

        to.value = newMap
    }

    private fun requestServiceDiscoveryFeature(onResultListener: ((result: WifiDirectActionListener.WifiDirectOperationResult) -> Unit)? = null){
        stopServicesDiscovery()

        val serviceDiscoveryFeatureRequest = WifiP2pDnsSdServiceRequest.newInstance()
        p2pManager.addServiceRequest(
            p2pChannel,
            serviceDiscoveryFeatureRequest,
            WifiDirectActionListener{ result ->
                if(result != WifiDirectActionListener.WifiDirectOperationResult.Success){
                    removeServiceDiscoveryFeatureRequests()
                }

                onResultListener?.invoke(result)
            })
    }

    /**
     * Remove all registered service discovery requests.
     */
    private fun removeServiceDiscoveryFeatureRequests(onResultListener: ((result: WifiDirectActionListener.WifiDirectOperationResult) -> Unit)? = null){
        p2pManager.clearServiceRequests(p2pChannel, onResultListener?.let { WifiDirectActionListener(it) })
    }

    private fun startServiceDiscoveryFeature(onResultListener: ((result: WifiDirectActionListener.WifiDirectOperationResult) -> Unit)? = null){
        p2pManager.discoverServices(p2pChannel, onResultListener?.let{ WifiDirectActionListener(it) })
    }

    companion object{
        private var singleton: WifiDirectServiceManager? = null

        fun getInstance(p2pManager: WifiP2pManager, p2pChannel: WifiP2pManager.Channel): WifiDirectServiceManager{
            return singleton ?: WifiDirectServiceManager(p2pManager, p2pChannel)
        }
    }

}