package me.michael.flora.wifidirect

import android.Manifest
import android.content.Context
import android.net.NetworkInfo
import android.net.wifi.WpsInfo
import android.net.wifi.p2p.*
import androidx.annotation.RequiresPermission
import androidx.lifecycle.LiveData
import me.michael.flora.wifidirect.service.WifiDirectService
import me.michael.flora.wifidirect.service.WifiDirectServiceManager

class WifiDirectManager private constructor(
    private val p2pManager: WifiP2pManager,
    private val p2pChannel: WifiP2pManager.Channel
) {
    private val wifiDirectServiceManager = WifiDirectServiceManager.getInstance(p2pManager, p2pChannel)
    val availableRemoteServices: LiveData<Map<String, WifiDirectService>> = wifiDirectServiceManager.availableRemoteServices
    val availableRemoteServicesWithRecords: LiveData<Map<String, WifiDirectService>> = wifiDirectServiceManager.availableRemoteServicesWithRecords

    /**
     * Initiate peer discovery. A discovery process involves scanning for available Wi-Fi peers
     * for the purpose of establishing a connection.
     *
     * The discovery remains active until a connection is initiated or a p2p group is formed.
     *
     * To fetch currently available peers use [requestPeers] after starting peers discovery
     */
    @RequiresPermission(Manifest.permission.ACCESS_FINE_LOCATION)
    fun startPeersDiscovery(onResultListener: ((result: WifiDirectActionListener.WifiDirectOperationResult) -> Unit)? = null){
        p2pManager.discoverPeers(p2pChannel, onResultListener?.let{ WifiDirectActionListener(it) })
    }

    /**
     * Request p2p discovery state.
     *
     * This state indicates whether p2p discovery has started or stopped.
     * The valid value is one of [WifiP2pManager.WIFI_P2P_DISCOVERY_STARTED]
     * or [WifiP2pManager.WIFI_P2P_DISCOVERY_STOPPED]
     */
    fun requestPeersDiscoveryState(onPeersDiscoveryStateReceived: (Int) -> Unit){
        p2pManager.requestDiscoveryState(p2pChannel, onPeersDiscoveryStateReceived)
    }

    /**
     * Stop an ongoing peer discovery
     */
    fun stopPeersDiscovery(onResultListener: ((result: WifiDirectActionListener.WifiDirectOperationResult) -> Unit)? = null){
        p2pManager.stopPeerDiscovery(p2pChannel, onResultListener?.let{ WifiDirectActionListener(it) })
    }

    /**
     * Request the current list of peers.
     */
    @RequiresPermission(Manifest.permission.ACCESS_FINE_LOCATION)
    fun requestPeers(onPeersReceived: ((peersList: WifiP2pDeviceList) -> Unit)) {
        p2pManager.requestPeers(p2pChannel, onPeersReceived)
    }

    /**
     * Start a p2p connection to the specified device.
     *
     * Register for [WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION] intent to determine
     * when the framework notifies of a change in connectivity.
     *
     * If the current device is not part of a p2p group, a connect request initiates
     * a group negotiation with the peer.
     *
     * If the current device is part of an existing p2p group or has created a p2p group with
     * [createGroup], an invitation to join the group is sent to the peer device.
     */
    @RequiresPermission(Manifest.permission.ACCESS_FINE_LOCATION)
    fun connect(peer: WifiP2pDevice, onResultListener: ((result: WifiDirectActionListener.WifiDirectOperationResult) -> Unit)? = null){
        val connectionConfig = getConnectionConfig(peer)

        p2pManager.connect(p2pChannel, connectionConfig, onResultListener?.let{ WifiDirectActionListener(it) })
    }

    /**
     * Create a p2p group with the current device as the group owner.
     * This essentially creates an access point that can accept connections
     * from legacy clients as well as other p2p devices.
     *
     * Note: This function would normally not be used unless the current device needs to form a p2p group
     * as a Group Owner and allow peers to join it as either Group Clients or legacy Wi-Fi STAs.
     */
    @RequiresPermission(Manifest.permission.ACCESS_FINE_LOCATION)
    fun createGroup(peer: WifiP2pDevice, onResultListener: ((result: WifiDirectActionListener.WifiDirectOperationResult) -> Unit)? = null){
        val connectionConfig = getConnectionConfig(peer)

        p2pManager.createGroup(p2pChannel, connectionConfig, onResultListener?.let{ WifiDirectActionListener(it) })
    }

    /**
     * Request device connection information.
     */
    fun requestConnectionInformation(onConnectionInformationChanged: (information: WifiP2pInfo) -> Unit){
        p2pManager.requestConnectionInfo(p2pChannel, onConnectionInformationChanged)
    }

    /**
     * Request p2p group information.
     */
    @RequiresPermission(Manifest.permission.ACCESS_FINE_LOCATION)
    fun requestGroupInformation(onGroupInformationAvailable: (WifiP2pGroup) -> Unit){
        p2pManager.requestGroupInfo(p2pChannel, onGroupInformationAvailable)
    }

    /**
     * Cancel any ongoing p2p group negotiation
     */
    fun disconnect(onResultListener: ((result: WifiDirectActionListener.WifiDirectOperationResult) -> Unit)? = null){
        p2pManager.cancelConnect(p2pChannel, onResultListener?.let{ WifiDirectActionListener(it) })
    }

    /**
     * Remove the current p2p group.
     */
    fun removeGroup(onResultListener: ((result: WifiDirectActionListener.WifiDirectOperationResult) -> Unit)? = null){
        p2pManager.removeGroup(p2pChannel, onResultListener?.let{ WifiDirectActionListener(it)})
    }

    /**
     * Register a local service for service discovery. If a local service is registered,
     * the framework automatically responds to a service discovery request from a peer.
     */
    @RequiresPermission(Manifest.permission.ACCESS_FINE_LOCATION)
    fun registerLocalService(service: WifiDirectService, onResultListener: ((result: WifiDirectActionListener.WifiDirectOperationResult) -> Unit)? = null){
        wifiDirectServiceManager.addLocalService(service, onResultListener)
    }

    /**
     * Remove a registered local service added with [registerLocalService]
     */
    fun unregisterLocalService(service: WifiDirectService, onResultListener: ((result: WifiDirectActionListener.WifiDirectOperationResult) -> Unit)? = null){
        wifiDirectServiceManager.removeLocalService(service, onResultListener)
    }

    /**
     * Initiate service discovery. A discovery process involves scanning for requested services for
     * the purpose of establishing a connection to a peer that supports an available service.
     */
    @RequiresPermission(Manifest.permission.ACCESS_FINE_LOCATION)
    fun startServicesDiscovery(onResultListener: ((result: WifiDirectActionListener.WifiDirectOperationResult) -> Unit)? = null){
        wifiDirectServiceManager.startServicesDiscovery(onResultListener)
    }

    /**
     * Cancel any ongoing service discovery.
     */
    fun stopServicesDiscovery(onResultListener: ((result: WifiDirectActionListener.WifiDirectOperationResult) -> Unit)? = null){
        wifiDirectServiceManager.stopServicesDiscovery(onResultListener)
    }

    /**
     * This method provides the device info in the form of a WifiP2pDevice.
     * Valid WifiP2pDevice is returned when p2p is enabled.
     * To get information notifications on P2P getting enabled refers WIFI_P2P_STATE_ENABLED.
     *
     * WifiP2pDevice.deviceAddress is only available if the caller holds
     * the android.Manifest.permission#LOCAL_MAC_ADDRESS permission,
     * and holds the anonymized MAC address (02:00:00:00:00:00) otherwise.
     *
     * This information is also included in the callback passed to
     * [WifiDirectBroadcastReceiver.Builder.addOnThisDeviceChangedListener]
     */
    @RequiresPermission(Manifest.permission.ACCESS_FINE_LOCATION)
    fun requestDeviceInformation(onInformationReceived: (WifiP2pDevice?) -> Unit){
        p2pManager.requestDeviceInfo(p2pChannel, onInformationReceived)
    }

    /**
     * Request network information
     *
     * This method provides the network info in the form of a NetworkInfo.
     * [NetworkInfo.isAvailable] indicates the p2p availability and NetworkInfo.getDetailedState()
     * reports the current fine-grained state of the network.
     *
     * This information is also included in the callback passed to
     * [WifiDirectBroadcastReceiver.Builder.addOnConnectionStateChangedListener]
     */
    fun requestNetworkInformation(onNetworkInfoReceived: (NetworkInfo) -> Unit){
        p2pManager.requestNetworkInfo(p2pChannel, onNetworkInfoReceived)
    }

    private fun getConnectionConfig(device: WifiP2pDevice): WifiP2pConfig{
        return WifiP2pConfig().apply {
            deviceAddress = device.deviceAddress
            wps.setup = WpsInfo.PBC
        }
    }

    companion object{
        fun getInstance(context: Context): WifiDirectManager?{
            val p2pManager = context.getSystemService(Context.WIFI_P2P_SERVICE) as? WifiP2pManager ?: return null
            val p2pChannel = p2pManager.initialize(context, context.mainLooper, null) ?: return null

            return WifiDirectManager(p2pManager, p2pChannel)
        }
    }
}