package me.michael.flora.p2pcommunication.entity

import java.util.*

data class Message (
    val senderName: String,
    val date: Date,
    val content: String
)