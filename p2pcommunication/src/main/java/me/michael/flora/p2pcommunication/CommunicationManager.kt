package me.michael.flora.p2pcommunication

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.google.android.gms.nearby.Nearby
import com.google.android.gms.nearby.connection.*
import me.michael.flora.p2pcommunication.entity.User
import me.michael.flora.p2pcommunication.entity.UserInformation
import java.util.*

class CommunicationManager private constructor(
    applicationContext: Context,
    private val serviceId: String
) {
    private val logTag = this::class.java.simpleName
    private val p2pStrategy = Strategy.P2P_CLUSTER
    private val connectionsClient: ConnectionsClient = Nearby.getConnectionsClient(applicationContext)
    private val availableEndpoints = mutableSetOf<String>()
    private val communicationChannelsList = mutableListOf<P2pCommunicationChannel>()
    private val thisEndpointName = Random().nextInt(9999) .toString()

    private var isCommunicationActive = false
    private lateinit var currentUserInformation: UserInformation

    private val mutableAvailableUsers = MutableLiveData<List<User>>(emptyList())

    val availableUsers: LiveData<List<User>> = mutableAvailableUsers

    private val endpointDiscoveryCallback = object: EndpointDiscoveryCallback(){
        override fun onEndpointFound(endpointId: String, endpointInformation: DiscoveredEndpointInfo) {
            Log.i(logTag, "Found new endpoint [ $endpointId ]")
            availableEndpoints.add(endpointId)

            val connectionAlreadyExists = communicationChannelsList.find { communicationChannel ->
                communicationChannel.endpointId == endpointId
            } != null

            if(connectionAlreadyExists){
                Log.i(logTag, "Connection to endpoint [ $endpointId ] already exists")
                return
            }

            requestConnection(endpointId)

        }

        override fun onEndpointLost(endpointId: String) {
            Log.i(logTag, "Endpoint [ $endpointId ] lost")
            availableEndpoints.remove(endpointId)
            removeUserByEndpointId(endpointId)
        }

    }

    private fun requestConnection(endpointId: String){
        connectionsClient.requestConnection(thisEndpointName, endpointId, connectionLifecycleListener)
            .addOnCompleteListener {
                Log.i(logTag, "Connection request to endpoint [ $endpointId ] completed")
            }
            .addOnSuccessListener {
                Log.i(logTag, "Successfully requested connection to endpoint [ $endpointId ]")
            }
            .addOnFailureListener {
                Log.e(logTag, "Connection request to endpoint [ $endpointId ] failed", it)

                if(availableEndpoints.any { availableEndpoint -> availableEndpoint == endpointId }){
                    connectionsClient.disconnectFromEndpoint(endpointId)
                    requestConnection(endpointId)
                }
            }
    }

    private val connectionLifecycleListener
        get() = object: ConnectionLifecycleCallback(){
            lateinit var p2PCommunicationChannel: P2pCommunicationChannel

            override fun onConnectionInitiated(endpointId: String, connectionInfo: ConnectionInfo) {
                Log.i(logTag, "Initiated connection to endpoint [ $endpointId ]")

                p2PCommunicationChannel = P2pCommunicationChannel(endpointId, connectionsClient)

                connectionsClient.acceptConnection(endpointId, p2PCommunicationChannel.payloadCallback)
            }

            override fun onConnectionResult(endpointId: String, resolution: ConnectionResolution) {
                if(resolution.status.isSuccess){
                    Log.i(logTag, "Connection is successfully established to endpoint [ $endpointId ]")

                    communicationChannelsList.add(p2PCommunicationChannel)

                    p2PCommunicationChannel.sendUserInformation(currentUserInformation)

                    p2PCommunicationChannel.receivedUserInformation.observeForever(
                        object: Observer<UserInformation>{
                            override fun onChanged(userInformation: UserInformation?) {
                                userInformation?.let {
                                    val newUser = User(userInformation, p2PCommunicationChannel)

                                    addNewUser(newUser)

                                    p2PCommunicationChannel.receivedUserInformation.removeObserver(this)
                                }
                            }
                        }
                    )
                } else {
                    Log.e(logTag, "Failed to establish connection to endpoint [ $endpointId ] due to: ${resolution.status.statusCode}")
                }
            }

            override fun onDisconnected(endpointId: String) {
                Log.i(logTag, "Started disconnection from endpoint [ $endpointId ]")

                communicationChannelsList.remove(p2PCommunicationChannel)
                Log.i(logTag, "Removed communication channel")

                removeUserByEndpointId(endpointId)

                Log.i(logTag, "Disconnected from endpoint [ $endpointId ]")
            }
        }

    fun enterWirelessNetwork(userInformation: UserInformation, onResultListener: ((started: Boolean) -> Unit)? = null){
        currentUserInformation = userInformation

        startCommunication(onResultListener)
    }

    fun leaveWirelessNetwork(){
        stopAdvertising()
        stopDiscovery()
        stopCommunication()
    }

    private fun startCommunication(onResultListener: ((started: Boolean) -> Unit)? = null){

        if(isCommunicationActive){
            onResultListener?.invoke(true)
            return
        }

        startAdvertising{ advertisingStarted ->
            if(!advertisingStarted){
                onResultListener?.invoke(advertisingStarted)
                isCommunicationActive = false
                return@startAdvertising
            }

            startDiscovery{ discoveryStarted ->
                if(!discoveryStarted) {
                    stopAdvertising()
                }

                isCommunicationActive = discoveryStarted
                onResultListener?.invoke(discoveryStarted)
            }
        }
    }

    private fun startAdvertising(onResultListener: (started: Boolean) -> Unit){
        val advertisingOptions = AdvertisingOptions.Builder()
            .setStrategy(p2pStrategy)
            .build()

        val advertisingTask = connectionsClient.startAdvertising(
            thisEndpointName,
            serviceId,
            connectionLifecycleListener,
            advertisingOptions)

        advertisingTask.addOnSuccessListener {
            Log.i(logTag, "Successfully started peer advertising")
            onResultListener(true)
        }

        advertisingTask.addOnFailureListener { exception ->
            Log.e(logTag, "Failed to start peer advertising", exception)
            onResultListener(false)
        }
    }

    private fun stopAdvertising(){
        connectionsClient.stopAdvertising()
    }

    private fun startDiscovery(onResultListener: (started: Boolean) -> Unit){
        val discoveryOptions = DiscoveryOptions.Builder()
            .setStrategy(p2pStrategy)
            .build()

        val discoveryTask = connectionsClient.startDiscovery(
            serviceId,
            endpointDiscoveryCallback,
            discoveryOptions
        )

        discoveryTask.addOnSuccessListener {
            Log.i(logTag, "Successfully started peers discovery")
            onResultListener(true)
        }

        discoveryTask.addOnFailureListener { exception ->
            Log.e(logTag, "Failed to start peers discovery", exception)
            onResultListener(false)
        }
    }

    private fun stopDiscovery(){
        connectionsClient.stopDiscovery()
    }

    private fun stopCommunication(){
        availableEndpoints.clear()
        connectionsClient.stopAllEndpoints()
        removeAllAvailableUsers()
        communicationChannelsList.clear()
        isCommunicationActive = false
    }

    private fun addNewUser(user: User){
        val newUsersList = mutableAvailableUsers.value?.toMutableList() ?: mutableListOf()
        newUsersList.add(user)
        mutableAvailableUsers.value = newUsersList

        Log.i(logTag, "New user added [ user name: ${user.userInformation.name}, endpoint: ${user.communicationChannel.endpointId}]")
    }

    private fun removeUser(user: User){
        val newUsersList = mutableAvailableUsers.value?.toMutableList() ?: mutableListOf()
        newUsersList.remove(user)
        mutableAvailableUsers.value = newUsersList

        Log.i(logTag, "User removed [ user name: ${user.userInformation.name}, endpoint: ${user.communicationChannel.endpointId}]")
    }

    private fun removeUserByEndpointId(endpointId: String){
        val userToRemove = availableUsers.value?.firstOrNull { it.communicationChannel.endpointId == endpointId }

        userToRemove?.let{
            removeUser(userToRemove)
        } ?: Log.i(logTag, "Failed to remove user: user does not exist [ endpointId: $endpointId ]")
    }

    private fun removeAllAvailableUsers(){
        mutableAvailableUsers.value = emptyList()

        Log.i(logTag, "Removed all available users")
    }

    companion object{
        private var singleton: CommunicationManager? = null

        fun getInstance(applicationContext: Context, serviceId: String = applicationContext.packageName): CommunicationManager{
            val instance = singleton ?: CommunicationManager(applicationContext, serviceId)
            singleton = instance

            return instance
        }
    }
}