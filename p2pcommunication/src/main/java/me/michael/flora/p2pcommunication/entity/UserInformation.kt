package me.michael.flora.p2pcommunication.entity

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class UserInformation(
    var name: String,
    var dateOfBirthday: Date,
    var sex: UserSex
): Parcelable