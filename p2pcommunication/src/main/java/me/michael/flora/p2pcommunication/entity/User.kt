package me.michael.flora.p2pcommunication.entity

import me.michael.flora.p2pcommunication.P2pCommunicationChannel

data class User (
    val userInformation: UserInformation,
    val communicationChannel: P2pCommunicationChannel
)