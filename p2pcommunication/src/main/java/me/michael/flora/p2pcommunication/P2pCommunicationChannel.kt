package me.michael.flora.p2pcommunication

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.nearby.connection.ConnectionsClient
import com.google.android.gms.nearby.connection.Payload
import com.google.android.gms.nearby.connection.PayloadCallback
import com.google.android.gms.nearby.connection.PayloadTransferUpdate
import com.google.gson.GsonBuilder
import me.michael.flora.p2pcommunication.entity.Message
import me.michael.flora.p2pcommunication.entity.UserInformation
import java.nio.charset.StandardCharsets

class P2pCommunicationChannel(
    val endpointId: String,
    private val connectionsClient: ConnectionsClient
){
    private val logTag = this::class.java.simpleName
    private val gson = GsonBuilder()
        .setPrettyPrinting()
        .setDateFormat("dd.MM.yyyy HH:mm:ss")
        .create()
    private val mutableReceivedUserInformation = MutableLiveData<UserInformation>()
    private val mutableReceivedMessage = MutableLiveData<Message>()

    val receivedMessage: LiveData<Message> = mutableReceivedMessage
    val receivedUserInformation: LiveData<UserInformation> = mutableReceivedUserInformation


    val payloadCallback = object: PayloadCallback(){
        override fun onPayloadReceived(endpointId: String, payload: Payload) {
            val receivedData = payload.asBytes()?.toString(StandardCharsets.UTF_8)

            Log.i(logTag,"Received data: $receivedData")

            receivedData?.run {
                handlePackage(this)
            }
        }

        override fun onPayloadTransferUpdate(endpointId: String, update: PayloadTransferUpdate) {
            when(update.status){
                PayloadTransferUpdate.Status.SUCCESS -> Log.i(logTag, "Payload transferred successfully")
                PayloadTransferUpdate.Status.IN_PROGRESS -> Log.i(logTag, "Payload transfer is in progress")
                PayloadTransferUpdate.Status.CANCELED -> Log.i(logTag, "Payload transfer was canceled")
                PayloadTransferUpdate.Status.FAILURE -> Log.e(logTag, "Payload transfer have been failed")
            }
        }
    }

    fun sendUserInformation(userInformation: UserInformation){
        sendPackage(USER_INFORMATION_TAG ,userInformation)
    }

    fun sendMessage(message: Message){
        sendPackage(MESSAGE_TAG ,message)
    }

    fun handlePackage(content: String){
        val packageBlocks = content.split("::")
        packageBlocks.getOrNull(0)?.toInt()?.let { tag ->
            packageBlocks.getOrNull(1)?.let { json ->

                when(tag){
                    USER_INFORMATION_TAG -> {
                        val userInformation = gson.fromJson<UserInformation>(json, UserInformation::class.java)
                        handleUserInformation(userInformation)
                    }
                    MESSAGE_TAG -> {
                        val message = gson.fromJson<Message>(json, Message::class.java)
                        handleMessage(message)
                    }
                    else -> null
                }
            }
        }

    }

    private fun handleUserInformation(userInformation: UserInformation){
        mutableReceivedUserInformation.value = userInformation
    }

    private fun handleMessage(message: Message){
        mutableReceivedMessage.value = message
    }

    private fun <T> sendPackage(tag: Int, content: T){

        val jsonMessage = gson.toJson(content)

        val networkPackage = "$tag::$jsonMessage"

        val payload = Payload.fromBytes(networkPackage.toByteArray())

        connectionsClient.sendPayload(endpointId, payload)

        Log.i(logTag,"Sent data: $networkPackage")
    }

    companion object{
        private const val USER_INFORMATION_TAG: Int = 1
        private const val MESSAGE_TAG: Int = 2


    }
}