package me.michael.flora.p2pcommunication.entity

enum class UserWish {
    JustChatting,
    NothingSerious,
    SeriousRelationship,
    DontKnow
}